
<?php

class Add_Icon {

	public $post_type = 'service';

	static $meta_key = 'service_icon';

	public function __construct() {
		add_action( 'add_meta_boxes', array( $this, 'add_metabox' ) );
	    add_action( 'save_post', array( $this, 'save_metabox' ) );
		//add_action( 'admin_print_footer_scripts', array( $this, 'show_assets' ), 10, 999 );
	}

	## Добавляет матабоксы
	public function add_metabox() {
		add_meta_box( 'box_icon_service', 'Миниатюра в заголовке', array( $this, 'render_metabox' ), $this->post_type, 'advanced', 'high' );
	}

	## Отображает метабокс на странице редактирования 
	public function render_metabox( $post ) {

     ?>
		<input type="hidden" name="extra_fields_nonce" value="<?php  echo wp_create_nonce(__FILE__); ?>" />	
		<label for="icon_input" >Миниатюра  </label>

     <?php

		$input = '
				  <input id="icon_input" type="text" name="'. self::$meta_key .'[]" value="%s">
				  ';

		$icon_service = get_post_meta( $post->ID, self::$meta_key, true );

		//var_dump($icon_service);

		if ( is_array( $icon_service ) ) {
			foreach ( $icon_service as $addr ) {
				printf( $input, esc_attr( $addr ) );
				}
		} else {
				printf( $input, '' );
		}
	}


	## Очищает и сохраняет значения полей
	public function save_metabox( $post_id ) {

				// Check if it's not an autosave.
			//	if ( wp_is_post_autosave( $post_id ) )
				//	return;

			//	базовая проверка
		if (
			   empty($_POST[self::$meta_key] )
			|| ! wp_verify_nonce( $_POST['extra_fields_nonce'], __FILE__ )
			|| wp_is_post_autosave( $post_id )
			|| wp_is_post_revision( $post_id )
			)
			return false;

            if ( isset( $_POST[self::$meta_key] ) && is_array( $_POST[self::$meta_key] ) ) {
					$icons_service = $_POST[self::$meta_key];

					$icons_service = array_map( 'sanitize_text_field', $icons_service ); // очистка

					$icons_service = array_filter( $icons_service ); // уберем пустые адреса

					if ( $icons_service ) 
				      update_post_meta( $post_id, self::$meta_key, $icons_service );
					else 
						delete_post_meta( $post_id, self::$meta_key );

				}

			}



			## Подключает скрипты и стили
			public function show_assets() {
				if ( is_admin() && get_current_screen()->id == $this->post_type ) {
					//$this->show_styles();
				//	$this->show_scripts();
				}
			}

			## Выводит на экран стили
			public function show_styles() {
		?>

		<!-- <style>
			.add-company-address {
				color: #00a0d2;
				cursor: pointer;
			}
			.company-address-list .item-address {
				display: flex;
				align-items: center;
			}
			.company-address-list .item-address input {
				width: 100%;
				max-width: 400px;
			}
			.remove-company-address {
				color: brown;
				cursor: pointer;
			}
		</style> -->
		<?php
	}


	## Выводит на экран JS
	public function show_scripts() {
		?>
		<script>
			jQuery(document).ready(function ($) {

				var $companyInfo = $('.company-info');

				// Добавляет бокс с вводом адреса фирмы
				$('.add-company-address', $companyInfo).click(function () {
					var $list = $('.company-address-list');
						$item = $list.find('.item-address').first().clone();

					$item.find('input').val(''); // чистим знанчение

					$list.append( $item );
				});

				// Удаляет бокс с вводом адреса фирмы
				$companyInfo.on('click', '.remove-company-address', function () {
					if ($('.item-address').length > 1) {
						$(this).closest('.item-address').remove();
					}
					else {
						$(this).closest('.item-address').find('input').val('');
					}
				});

			});
		</script>
<?php
	}

}
?>