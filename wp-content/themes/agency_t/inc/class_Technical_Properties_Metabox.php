
<?php

class Technical_Properties_Metabox {

	public $post_type = 'property';

	static $meta_key = 'technical_properties';

	public function __construct() {
		add_action( 'add_meta_boxes', array( $this, 'add_metabox' ) );
		add_action( 'save_post_' . $this->post_type, array( $this, 'save_metabox' ) );
		add_action( 'admin_print_footer_scripts', array( $this, 'show_assets' ), 10, 999 );
	}

	## Добавляет матабоксы
	public function add_metabox() {
		add_meta_box( 'box_technical_properties', 'Технические данные', array( $this, 'render_metabox' ), $this->post_type, 'advanced', 'high' );
	}

	## Отображает метабокс на странице редактирования поста
	public function render_metabox( $post ) {

		// Добавляем nonce поле, которое будем проверять при сохранении.
		wp_nonce_field( 'technical_properties_text_nonce', 'technical_properties_fields_nonce' );


		echo '<table class="form-table property-info">'; 
		echo 	'<thead>'; 
		echo 		'<tr>'; 
		echo 			'<th >Имя</th>'; 
		echo 			'<th >Значение</th>'; 
		echo 		'</tr>'; 
		echo 	'</thead>'; 
		echo 	'<tbody class="list-technical-properties">'; 
		echo 		'<tr>'; 
		echo 		  '<th colspan="2"> Добавить новые данные  <span class="dashicons dashicons-plus-alt add-technical-property"></span>   </th>'; 
		echo 		'</tr>'; 
		
//---------------------------------------

	    $input_name = '<tr class="item-technical-property"> <td>' ;
		$input_name .= '<span>
							<input class="name-technical-property" type="text" name="name_'. self::$meta_key .'[]" value="%s"> 
						 </span>
						 ' ;

        $input_value  = '</td> <td> ';
        $input_value  .= '<span>
						     <input class="value-technical-property" type="text" name="value_'. self::$meta_key .'[]" value="%s">
						      <span class="dashicons dashicons-trash remove-item-technical-property"></span>
						 </span>		
						 ';

		$input_value .= ' </td> </tr>';
        
        $list_technical_properties = get_post_meta( $post->ID, self::$meta_key, true );
      
      // var_dump($list_technical_properties);

      if ( is_array( $list_technical_properties ) ) {  


      	for ( $i = 0; $i < count($list_technical_properties['name']); $i++) { 	
          printf( $input_name,  esc_attr($list_technical_properties['name'][$i]) );
          printf( $input_value,  esc_attr($list_technical_properties['value'][$i]) );	
        }  // end for()
       
      } else {
		  printf( $input_name, '' );	
		  printf( $input_value, '' );	
	  }  // End If()
//---------------------------------------
		echo 	'</tbody>'; 
		echo '</table>'; 
			
	}			
	

	## Очищает и сохраняет значения полей
	public function save_metabox( $post_id ) {

		        $array_technical = array();
		        $technical = array();

				// Check if it's not an autosave.
				if ( wp_is_post_autosave( $post_id ) )
					return;




				if ( isset( $_POST['name_'.self::$meta_key] ) && is_array( $_POST['name_'.self::$meta_key] ) && isset( $_POST['value_'.self::$meta_key] ) && is_array( $_POST['value_'.self::$meta_key] ) ) {

				
					$technical['name'] = $_POST['name_'.self::$meta_key];
					$technical['value'] = $_POST['value_'.self::$meta_key];
					
					// очистка
					$technical['name'] = array_map( 'sanitize_text_field', $technical['name'] ); 
					$technical['value'] = array_map( 'sanitize_text_field', $technical['value'] ); 

					// уберем пустые 
					$technical['name'] = array_filter( $technical['name'] ); 
					// уберем пустые 
					$technical['value'] = array_filter( $technical['value'] ); 

						
					if (   $technical['name'] && $technical['value'] ) {
						update_post_meta( $post_id, self::$meta_key, $technical  );
					}
					else 
						delete_post_meta( $post_id, self::$meta_key );

				}
			}

			## Подключает скрипты и стили
			public function show_assets() {
				if ( is_admin() && get_current_screen()->id == $this->post_type ) {
					$this->show_styles();
					$this->show_scripts();
				}
			}

			## Выводит на экран стили
			public function show_styles() {
		?>
		<style>
			.add-technical-property {
				color: #00a0d2;
				cursor: pointer;
			}
			 .item-technical-property {
				align-items: center;
			}
			 .item-technical-property input,
			  {
				width: 100%;
				max-width: 400px;
			}
			.remove-item-technical-property {
				color: brown;
				cursor: pointer;
			}
		</style>
		<?php
	}


	## Выводит на экран JS
	public function show_scripts() {
		?>
		<script>
			jQuery(document).ready(function ($) {

				var $tablePropertyInfo = $('.property-info');

				// Добавляет бокс с вводом адреса фирмы
				$('.add-technical-property', $tablePropertyInfo).click(function () {

					var $list = $('.list-technical-properties');

				//	console.log('List:', $list);

	$item = $list.find('.item-technical-property').first().clone();
				
	$item.find('input.name-technical-property').val(''); // чистим знанчение
	$item.find('input.value-technical-property').val(''); // чистим знанчение

	//console.log('Item name:', $item);

				$list.append($item );

				});

				// Удаляет бокс с вводом из фирмы

				$tablePropertyInfo.on('click', '.remove-item-technical-property', function () {

					if ($('.item-technical-property').length > 1) {
						$(this).closest('.item-technical-property').remove();
					}
					else {
					$(this).closest('.item-technical-property').find('input .name-technical-property').val('');
					$(this).closest('.item-technical-property').find('input .value-technical-property').val('');
					}
				});

			});
		</script>
<?php
	}

}
?>