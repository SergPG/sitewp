
<?php

class Adm_Property {

	public $post_type = 'property';

	static $meta_keys = array(
		'property_rent' => 'Rent',// Цена Рента
		'property_location' => 'Location',
		'property_type' => 'Property Type',
		'property_status' => 'Status',
		'property_area' => 'Area',
		'property_beds' => 'Beds',
		'property_baths' => 'Baths',
		'property_garage' => 'Garage'
		);


	public function __construct() {
		add_action( 'add_meta_boxes', array( $this, 'add_metabox' ) );
	    add_action( 'save_post', array( $this, 'save_metabox' ) );
		add_action( 'admin_print_footer_scripts', array( $this, 'show_assets' ), 10, 999 );
	}

	## Добавляет матабоксы
	public function add_metabox() {
		add_meta_box( 'box_info_property_house', 'Информ Карта', array( $this, 'render_metabox' ), $this->post_type, 'advanced', 'high' );
	}

	## Отображает метабокс на странице редактирования 
	public function render_metabox( $post ) {

		// Добавляем nonce поле, которое будем проверять при сохранении.
		wp_nonce_field( 'property_extra_fields_nonce', 'property_fields_nonce' );

     ?>
		


        <table class="form-table property-info">
			<thead>
				<tr>
			      <th >Имя</th>
			      <th >Значение</th>
			    </tr>
			</thead>
			<tbody>
				

		<?php foreach (self::$meta_keys  as $meta_key => $meta_name): ?>
			<tr>
	          <th> <?php echo $meta_name; ?> </th>	
			  <td> 
			  	<?php 
			  	  $input = '
				  <input id="icon_input" type="text" name="'.$meta_key .'[]" value="%s"> ';

					$property_meta = get_post_meta( $post->ID, $meta_key, true );

					if ( is_array( $property_meta ) ) {
						foreach ( $property_meta as $addr ) {
							printf( $input, esc_attr( $addr ) );
							}
					} else {
							printf( $input, '' );
					}
				?>				
			  </td>     
			</tr>
		<?php endforeach ?>

			</tbody>	
        </table>	

    <?php
	}


	## Очищает и сохраняет значения полей
	public function save_metabox( $post_id ) {


		// Проверяем установлен ли nonce.
		if ( ! isset( $_POST['property_fields_nonce'] ) )
			return $post_id;

		$nonce = $_POST['property_fields_nonce'];

		// Проверяем корректен ли nonce.
		if ( ! wp_verify_nonce( $nonce, 'property_extra_fields_nonce' ) )
			return $post_id;
        
        // Если это автосохранение ничего не делаем.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
			return $post_id;


        foreach (self::$meta_keys  as $meta_key => $meta_name){

           	if ( isset( $_POST[$meta_key] ) && is_array( $_POST[$meta_key] ) ) {
					$property_meta_value = $_POST[$meta_key];

					$property_meta_value = array_map( 'sanitize_text_field', $property_meta_value ); // очистка

					$property_meta_value = array_filter( $property_meta_value ); // уберем пустые адреса

					if ( $property_meta_value ) 
				      update_post_meta( $post_id, $meta_key, $property_meta_value );
					else 
						delete_post_meta( $post_id, $meta_key );

				} // end if
           }  // end foreach

	} // END save_metabox



			## Подключает скрипты и стили
			public function show_assets() {
				if ( is_admin() && get_current_screen()->id == $this->post_type ) {
					$this->show_styles();
				//	$this->show_scripts();
				}
			}

			## Выводит на экран стили
			public function show_styles() {
				?>

				<style>

					table.property-info thead{
						background: #cccfff;
					}

					table.property-info thead th,
					table.property-info tbody th,
					table.property-info tbody td {
						padding: 15px; /* Поля вокруг содержимого таблицы */
						text-align: center;
						color: #333;
					}

					
					table.property-info tbody th,
					table.property-info tbody td {
						border-bottom: 1px solid maroon; /* Параметры рамки */

						
						
					}
					
				</style>
			<?php
			}


	## Выводит на экран JS
	public function show_scripts() {
		?>
		<script>
			jQuery(document).ready(function ($) {

				var $companyInfo = $('.company-info');

				// Добавляет бокс с вводом адреса фирмы
				$('.add-company-address', $companyInfo).click(function () {
					var $list = $('.company-address-list');
						$item = $list.find('.item-address').first().clone();

					$item.find('input').val(''); // чистим знанчение

					$list.append( $item );
				});

				// Удаляет бокс с вводом адреса фирмы
				$companyInfo.on('click', '.remove-company-address', function () {
					if ($('.item-address').length > 1) {
						$(this).closest('.item-address').remove();
					}
					else {
						$(this).closest('.item-address').find('input').val('');
					}
				});

			});
		</script>
<?php
	}

}
?>