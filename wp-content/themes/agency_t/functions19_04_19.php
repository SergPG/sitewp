<?php
/**
 * agency_thm functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package agency_thm
 */

if ( ! function_exists( 'agency_thm_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function agency_thm_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on agency_thm, use a find and replace
		 * to change 'agency_thm' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'agency_thm', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-main' => esc_html__( 'Main Menu Top', 'agency_thm' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'agency_thm_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'agency_thm_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function agency_thm_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'agency_thm_content_width', 640 );
}
add_action( 'after_setup_theme', 'agency_thm_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function agency_thm_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'agency_thm' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'agency_thm' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'agency_thm_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function agency_thm_scripts() {
    
    //<!-- Google Fonts -->
    wp_enqueue_style( 'agency_thm-fonts_googleapis','https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700' );

    //<!-- Bootstrap CSS File -->
    wp_enqueue_style( 'agency_thm-bootstrap', get_template_directory_uri() . '/assets/lib/bootstrap/css/bootstrap.min.css' );

     //<!-- Libraries CSS Files -->
    wp_enqueue_style( 'agency_thm-font-awesome', get_template_directory_uri() . '/assets/lib/font-awesome/css/font-awesome.min.css' );
	wp_enqueue_style( 'agency_thm-animate', get_template_directory_uri() . '/assets/lib/animate/animate.min.css' );
	wp_enqueue_style( 'agency_thm-ionicons', get_template_directory_uri() . '/assets/lib/ionicons/css/ionicons.min.css' );
	wp_enqueue_style( 'agency_thm-owl_carousel', get_template_directory_uri() . '/assets/lib/owlcarousel/assets/owl.carousel.min.css' );

    //<!-- Main Stylesheet File -->
	wp_enqueue_style( 'agency_thm-style', get_stylesheet_uri() );

//===================================================================
	//<!-- JavaScript Libraries -->
 // <script src="lib/jquery/jquery.min.js"></script>
  
	//<-- jQuery -->
    wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'agency_thm-jquery-migrate', get_template_directory_uri() . '/assets/lib/jquery/jquery-migrate.min.js', array('jquery'), '20151215', true );
	wp_enqueue_script( 'agency_thm-popper', get_template_directory_uri() . '/assets/lib/popper/popper.min.js', array('jquery'), '20151215', true );
	wp_enqueue_script( 'agency_thm-bootstrap-js', get_template_directory_uri() . '/assets/lib/bootstrap/js/bootstrap.min.js', array('jquery'), '20151215', true );
	wp_enqueue_script( 'agency_thm-easing-js', get_template_directory_uri() . '/assets/lib/easing/easing.min.js', array('jquery'), '20151215', true );
	wp_enqueue_script( 'agency_thm-owlcarousel-js', get_template_directory_uri() . '/assets/lib/owlcarousel/owl.carousel.min.js', array('jquery'), '20151215', true );
	wp_enqueue_script( 'agency_thm-scrollreveal-js', get_template_directory_uri() . '/assets/lib/scrollreveal/scrollreveal.min.js', array('jquery'), '20151215', true );

      //<!-- Contact Form JavaScript File -->
    wp_enqueue_script( 'agency_thm-contactform-js', get_template_directory_uri() . '/assets/contactform/contactform.js', array('jquery'), '20151215', true );

	  //<!-- Template Main Javascript File -->
  	wp_enqueue_script( 'agency_thm-main-js', get_template_directory_uri() . '/js/main.js', array('jquery'), '20151215', true );

	wp_enqueue_script( 'agency_thm-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'agency_thm-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'agency_thm_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}


// // ДОбавляем классы ссылкам
// add_filter( 'nav_menu_link_attributes', 'filter_nav_menu_link_attributes', 10, 4 );
// function filter_nav_menu_link_attributes( $atts, $item, $args, $depth ) {
// 	if ( $args->theme_location === 'menu-main' ) {
		
// 		$atts['class'] = '';

// 		if ( $item->current ) {
// 			$atts['class'] .= ' active';
// 		}
// 	}

// 	return $atts;
// }



//=======



// //=========    menu-main' => 'Main Menu Top'======================
/*
*  Menu Bootstrap 4
  
		wp_nav_menu( array(
          'theme_location'  => 'menu-main',
          'container'       => false,
          'menu_class'      => '',
          'fallback_cb'     => '__return_false',
          'items_wrap'      => '<ul id="%1$s" class="navbar-nav %2$s">%3$s</ul>',
          'depth'           => 2,
          'walker'          => new bootstrap_4_walker_nav_menu(),
        ) );
*/

	class bootstrap_4_walker_nav_menu extends Walker_Nav_menu {
    
    function start_lvl( &$output, $depth = 0, $args = array() ){ // ul

        $indent = str_repeat("\t",$depth); // indents the outputted HTML
        $submenu = ($depth > 0) ? ' sub-menu' : '';
        $output .= "\n$indent<ul class=\"dropdown-menu$submenu depth_$depth\">\n";

    }
  
	  function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ){ // li a span
	
	    $indent = ( $depth ) ? str_repeat("\t",$depth) : '';
	    
	    $li_attributes = '';
	        $class_names = $value = '';
	    
	        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
	        
	        $classes[] = ($args->walker->has_children) ? 'dropdown' : '';
	        $classes[] = ($item->current || $item->current_item_anchestor) ? 'active' : '';
	        $classes[] = 'nav-item';
	        $classes[] = 'nav-item-' . $item->ID;
	        if( $depth && $args->walker->has_children ){
	            $classes[] = 'dropdown-menu';
	        }
	        
	        $class_names =  join(' ', apply_filters('nav_menu_css_class', array_filter( $classes ), $item, $args ) );
	        $class_names = ' class="' . esc_attr($class_names) . '"';
	        
	        $id = apply_filters('nav_menu_item_id', 'menu-item-'.$item->ID, $item, $args);
	        $id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';
	        
	        $output .= $indent . '<li ' . $id . $value . $class_names . $li_attributes . '>';
	        
	        $attributes = ! empty( $item->attr_title ) ? ' title="' . esc_attr($item->attr_title) . '"' : '';
	        $attributes .= ! empty( $item->target ) ? ' target="' . esc_attr($item->target) . '"' : '';
	        $attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr($item->xfn) . '"' : '';
	        $attributes .= ! empty( $item->url ) ? ' href="' . esc_attr($item->url) . '"' : '';



			$class_link = 'nav-link';

 			// if ($item->current || $item->current_item_anchestor) {

 			// 	$attributes .= ' class=" active " ';
             	 
    //          }



             if (!$depth && $args->walker->has_children ) {
             	$attributes .= ' class=" '.$class_link.'  dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"';  	

             	//var_dump($class_link);
             }
             elseif ($depth > 0) {
             	$attributes .= '  ';
             }
             else{
                 $attributes .= ' class=" '.$class_link.' " ';
             }

	        
            
	      //   $attributes .= ($item->current) ? ' class="active" ' : '';





          
           // var_dump($depth);
           // var_dump($args->walker->has_children);
	         
	        
	        $item_output = $args->before;
	        $item_output .= ( $depth > 0 ) ? '<a class="dropdown-item"' . $attributes . '>' : '<a' . $attributes . '>';
	        $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
	        $item_output .= '</a>';
	        $item_output .= $args->after;
	        
	        $output .= apply_filters ( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	    
	    }
	    
	}

?>