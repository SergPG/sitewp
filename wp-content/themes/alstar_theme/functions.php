<?php
/**
 * alstar_theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package alstar_theme
 */

if ( ! function_exists( 'alstar_theme_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function alstar_theme_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on alstar_theme, use a find and replace
		 * to change 'alstar_theme' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'alstar_theme', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-main' => esc_html__( 'Main Top Menu', 'alstar_theme' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'alstar_theme_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'alstar_theme_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function alstar_theme_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'alstar_theme_content_width', 640 );
}
add_action( 'after_setup_theme', 'alstar_theme_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function alstar_theme_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'alstar_theme' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'alstar_theme' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'alstar_theme_widgets_init' );


// Новый тип записи Section_type

add_action('init', 'type_post_section_type');

function type_post_section_type() {

	register_post_type('section_type', array(
		'labels'             => array(
			'name'               => 'Секции', // Основное название типа записи
			'singular_name'      => 'Секция', // отдельное название записи типа section_type
			'add_new'            => 'Добавить новую',
			'add_new_item'       => 'Добавить новую секцию',
			'edit_item'          => 'Редактировать секцию',
			'new_item'           => 'Новая секция',
			'view_item'          => 'Посмотреть секцию',
			'search_items'       => 'Найти секцию',
			'not_found'          =>  'Секция не найдена',
			'not_found_in_trash' => 'В корзине секций не найдено',
			'parent_item_colon'  => '',
			'menu_name'          => 'Секции'

		  ),
		'description'         => 'Секции на странице',
		'public'              => true,
		'publicly_queryable'  => true, // зависит от public
		'exclude_from_search' => true, // зависит от public
		'show_ui'             => true, // зависит от public
		'show_in_menu'        => true, // показывать ли в меню адмнки
		'show_in_admin_bar'   => true, // по умолчанию значение show_in_menu
		'show_in_nav_menus'   => true, // зависит от public
		'show_in_rest'        => true, // добавить в REST API. C WP 4.7
		'rest_base'           => null, // $post_type. C WP 4.7
		'menu_position'       => 4,
		'menu_icon'  		  => 'dashicons-images-alt',  // dashicons-admin-multisite
		//'capability_type'   => 'post',
		//'capabilities'      => 'post', // массив дополнительных прав для этого типа записи
		//'map_meta_cap'      => null, // Ставим true чтобы включить дефолтный обработчик специальных прав
		'hierarchical'        => true,
		'supports'            => array('title','editor','page-attributes'), // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
		'taxonomies'          => array(),
		'has_archive'         => false,
		'rewrite'             => true,
		'query_var'           => true,
	) );
}




// Remove themes old version of jQuery and load a compatible version
function my_update_jquery () {
	if ( !is_admin() ) { 
	   wp_deregister_script('jquery');
 	   wp_register_script('jquery', get_theme_file_uri('/assets/js/jquery.js'), false, false, true);
	   wp_enqueue_script('jquery');
	}
}
add_action('wp_enqueue_scripts', 'my_update_jquery');


/**
 * Enqueue scripts and styles.
 */
function alstar_theme_scripts() {

    wp_enqueue_style( 'alstar_theme-style', get_stylesheet_uri() );

	wp_enqueue_style( 'alstar_theme-fonts-googleapis','https://fonts.googleapis.com/css?family=Montserrat:400,700|Open+Sans:400,300,700,800' );


    //<!-- Bootstrap CSS File -->
    wp_enqueue_style( 'alstar_theme-bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css' );

      wp_enqueue_style( 'alstar_theme-main-style', get_template_directory_uri() . '/assets/css/style.css' );

    wp_enqueue_style( 'alstar_theme-color-default', get_template_directory_uri() . '/assets/color/default.css' );


  	//<-- jQuery -->
   // wp_enqueue_script( 'jquery' );


    wp_enqueue_script( 'alstar_theme_js-bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array('jquery'), '20151215', true );
   
    wp_enqueue_script( 'alstar_theme_js-wow', get_template_directory_uri() . '/assets/js/wow.min.js', array('jquery'), '20151215', true );
   
    wp_enqueue_script( 'alstar_theme_js-scrollTo', get_template_directory_uri() . '/assets/js/jquery.scrollTo.min.js', array('jquery'), '20151215', true );
    wp_enqueue_script( 'alstar_theme_js-nav', get_template_directory_uri() . '/assets/js/jquery.nav.js', array('jquery'), '20151215', true );
    
    wp_enqueue_script( 'alstar_theme_js-modernizr', get_template_directory_uri() . '/assets/js/modernizr.custom.js', array('jquery'), '20151215', true );
   
    wp_enqueue_script( 'alstar_theme_js-grid', get_template_directory_uri() . '/assets/js/grid.js', array('jquery'), '20151215', true );
   
    wp_enqueue_script( 'alstar_theme_js-stellar', get_template_directory_uri() . '/assets/js/stellar.js', array('jquery'), '20151215', true );

    //   <!-- Contact Form JavaScript File -->
	 wp_enqueue_script( 'alstar_theme_js-contactform', get_template_directory_uri() . '/assets/contactform/contactform.js', array('jquery'), '20151215', true ); 
	 //    <!-- Template Custom Javascript File -->
	  wp_enqueue_script( 'alstar_theme_js-main-custom', get_template_directory_uri() . '/assets/js/custom.js', array('jquery'), '20151215', true ); 




	wp_enqueue_script( 'alstar_theme-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'alstar_theme-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'alstar_theme_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}


add_filter( 'nav_menu_link_attributes', 'filter_function_main_menu', 10, 4 );

function filter_function_main_menu( $atts, $item, $args, $depth ){
	// filter...
   if ( $args->theme_location === 'menu-main' ) {



//    	$path = '/www/public_html/index.html';
// $filename = substr(strrchr($path, "/"), 1);
// echo $filename; // "index.html"

     if( ! empty( $atts['href'] ) ){

      $str_url = $atts['href'];
     
      $srt_len = iconv_strlen ($str_url);
      
      $pos = strrpos($str_url, "/");

      if($srt_len === ( $pos + 1)){
      	    $rest = substr($str_url, 0, -1);

      	    $new_str = substr(strrchr($rest, "/"), 1);

			$atts['href'] = '#'. esc_attr($new_str) ;
			
			var_dump($new_str);
      }
      else{

      	$atts['href'] = $item->url;
      }

    // var_dump($str_url,  $pos, $srt_len);
     	

     //	$atts['href'] = '#'.  substr(strrchr($item->url, "/"), 1);

    // 	$atts['href'] = str_replace( home_url(), '', $item->url );

    	
     }

   } // End If


	return $atts;
}


function change_default_placeholders( $placeholder ){
	$screen = get_current_screen();
	switch($screen->post_type){
		case 'post':{ // для постов
			$placeholder = 'Как назовём пост?';
			break;
		}
		case 'page':{ // для страниц
			$placeholder = 'Как назовём страницу?';
			break;
		}
		case 'section_type':{ // для игр (созданный тип постов)
			$placeholder = 'Введите название Секции';
			break;
		}
		// сюда можно добавить ещё сколько угодно условий
	}
	return $placeholder;
}
 
add_filter( 'enter_title_here', 'change_default_placeholders');


add_filter( 'default_content', 'custom_post_type_content' );
function custom_post_type_content( $content ) {
	if( function_exists('get_current_screen') && get_current_screen()->post_type == 'section_type') {
		$content = 'Контент по умолчанию для постов типа: "my_post"';
		return $content;
	}
}

